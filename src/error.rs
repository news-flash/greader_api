use crate::models::GReaderError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Failed to Parse URL")]
    Url(#[from] url::ParseError),
    #[error("Failed to (de)serialize Json")]
    Json {
        source: serde_json::error::Error,
        json: String,
    },
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("GReader specific error")]
    GReader(GReaderError),
    #[error("Bad input data")]
    BadRequest,
    #[error("Malformed input arguments")]
    Input,
    #[error("No valid access token available")]
    Token,
    #[error("Request failed with message access denied")]
    AccessDenied,
    #[error("Access token expired")]
    TokenExpired,
    #[error("API Limit reached for today")]
    ApiLimit,
    #[error("Error parsing string (enum/date) returned by GReader")]
    Parse,
    #[error("Not Logged in")]
    NotLoggedIn,
    #[error("{0}")]
    Other(String),
}

impl ApiError {
    pub fn parse_error(error: GReaderError) -> Self {
        if error
            .errors
            .iter()
            .any(|err| err.starts_with("token expired"))
        {
            Self::TokenExpired
        } else {
            Self::GReader(error)
        }
    }
}
