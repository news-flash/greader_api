use serde::Deserialize;
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize)]
pub struct StreamPrefs {
    pub streamprefs: HashMap<String, Vec<StreamPref>>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct StreamPref {
    pub id: String,
    pub value: String,
}
