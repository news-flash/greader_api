use chrono::{DateTime, Duration, Utc};

#[derive(Clone, Debug)]
pub struct PostToken {
    pub token: String,
    pub timestamp: DateTime<Utc>,
}

impl PostToken {
    pub fn new(token: &str) -> Self {
        Self {
            token: token.into(),
            timestamp: Utc::now(),
        }
    }

    pub fn is_valid(&self) -> bool {
        Utc::now() - self.timestamp < Duration::try_minutes(30).unwrap()
    }

    pub fn update(&mut self, token: &str) {
        self.token = token.into();
        self.timestamp = Utc::now();
    }
}
