use crate::error::ApiError;
use std::convert::{From, TryFrom};

#[derive(Copy, Clone)]
pub enum StreamType {
    Stream,
    Category,
}

impl From<StreamType> for &str {
    fn from(item: StreamType) -> Self {
        match item {
            StreamType::Stream => "s",
            StreamType::Category => "t",
        }
    }
}

impl TryFrom<&str> for StreamType {
    type Error = ApiError;

    fn try_from(item: &str) -> Result<Self, ApiError> {
        let parsed = match item {
            "s" => StreamType::Stream,
            "t" => StreamType::Category,
            _ => return Err(ApiError::Parse),
        };
        Ok(parsed)
    }
}

#[derive(Copy, Clone)]
pub enum State {
    #[cfg(feature = "freshrss")]
    All,
    Starred,
    Broadcast,
    Read,
    KeptUnread,
    Like,
    SavedWebPages,
}

// user/-/state/com.google/broadcast
// user/-/state/com.google/broadcast-friends-comments
// user/-/state/com.google/broadcast-friends

impl From<State> for &str {
    fn from(item: State) -> Self {
        match item {
            #[cfg(feature = "freshrss")]
            State::All => "user/-/state/com.google/reading-list",
            State::Starred => "user/-/state/com.google/starred",
            State::Broadcast => "user/-/state/com.google/broadcast",
            State::Read => "user/-/state/com.google/read",
            State::KeptUnread => "user/-/state/com.google/kept-unread",
            State::Like => "user/-/state/com.google/like",
            State::SavedWebPages => "user/-/state/com.google/saved-web-pages",
        }
    }
}

impl TryFrom<&str> for State {
    type Error = ApiError;

    fn try_from(item: &str) -> Result<Self, ApiError> {
        let parsed = match item {
            #[cfg(feature = "freshrss")]
            "user/-/state/com.google/reading-list" => State::All,
            "user/-/state/com.google/starred" => State::Starred,
            "user/-/state/com.google/broadcast" => State::Broadcast,
            "user/-/state/com.google/read" => State::Read,
            "user/-/state/com.google/kept-unread" => State::KeptUnread,
            "user/-/state/com.google/like" => State::Like,
            "user/-/state/com.google/saved-web-pages" => State::SavedWebPages,
            _ => return Err(ApiError::Parse),
        };
        Ok(parsed)
    }
}
