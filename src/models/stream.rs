use super::Enclosure;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Summary {
    pub content: String,
    pub direction: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Alternate {
    pub href: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Own {
    pub href: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Origin {
    pub stream_id: String,
    pub title: String,
    pub html_url: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Item {
    pub origin: Origin,
    pub updated: Option<i64>,
    pub id: String,
    pub categories: Vec<String>,
    pub author: Option<String>,
    pub alternate: Vec<Alternate>,
    pub timestamp_usec: String,
    pub crawl_time_msec: String,
    pub published: i64,
    pub title: Option<String>,
    #[serde(alias = "summary")]
    pub content: Option<Summary>,
    pub enclosure: Option<Vec<Enclosure>>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Stream {
    pub direction: Option<String>,
    pub id: String,
    pub title: Option<String>,
    pub description: Option<String>,
    // r#self (raw identifier) could not be used, therefore i gets renamed to own
    #[serde(rename = "self")]
    pub own: Option<Own>,
    pub updated: i64,
    pub updated_usec: Option<String>,
    pub items: Vec<Item>,
    pub author: Option<String>,
    pub continuation: Option<String>,
}

impl Summary {
    pub fn decompose(self) -> (String, Option<String>) {
        (self.content, self.direction)
    }
}

impl Alternate {
    pub fn decompose(self) -> String {
        self.href
    }
}

impl Origin {
    pub fn decompose(self) -> (String, String, Option<String>) {
        (self.stream_id, self.title, self.html_url)
    }
}
