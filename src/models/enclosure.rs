use crate::deserialize::to_optional_i64;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Enclosure {
    pub href: String,
    #[serde(rename = "type")]
    pub _type: Option<String>,
    #[serde(default)]
    #[serde(deserialize_with = "to_optional_i64")]
    pub length: Option<i64>,
}
