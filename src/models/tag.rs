use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Tagging {
    pub id: String,
    pub r#type: Option<String>,
    pub sortid: Option<String>,
    #[cfg(feature = "inoreader")]
    pub unread_count: Option<u64>,
    #[cfg(feature = "inoreader")]
    pub unseen_count: Option<u64>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Taggings {
    pub tags: Vec<Tagging>,
}
