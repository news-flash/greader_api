use chrono::{DateTime, Utc};
use serde::Deserialize;

use super::PostToken;

#[derive(Clone, Debug)]
pub enum AuthInput {
    Uninitialized,
    Inoreader(InoreaderAuthInput),
    Google(GoogleAuthInput),
}

#[derive(Clone, Debug)]
pub struct InoreaderAuthInput {
    pub auth_code: String,
    pub redirect_url: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Clone, Debug)]
pub struct GoogleAuthInput {
    pub username: String,
    pub password: String,
}

#[derive(Clone, Debug)]
pub enum AuthData {
    Uninitialized,
    Inoreader(InoreaderAuth),
    Google(GoogleAuth),
}

#[derive(Clone, Debug)]
pub struct InoreaderAuth {
    pub client_id: String,
    pub client_secret: String,
    pub access_token: String,
    pub refresh_token: String,
    pub expires_at: DateTime<Utc>,
}

#[derive(Clone, Debug)]
pub struct GoogleAuth {
    pub username: String,
    pub password: String,
    pub auth_token: Option<String>,
    pub post_token: Option<PostToken>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct OAuthResponse {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: i64,
    pub refresh_token: String,
    pub scope: String,
}
