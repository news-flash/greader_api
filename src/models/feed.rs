use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Category {
    pub id: String,
    pub label: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Feed {
    pub id: String,
    pub title: String,
    pub categories: Vec<Category>,
    pub url: String,
    pub html_url: String,
    pub icon_url: String,
    pub sortid: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Feeds {
    pub subscriptions: Vec<Feed>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct QuickFeed {
    pub num_results: u64,
    pub query: String,
    pub stream_id: String,
    pub stream_name: Option<String>,
}
