use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct ItemId {
    pub id: String,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ItemRefs {
    pub item_refs: Option<Vec<ItemId>>,
    pub continuation: Option<String>,
}
