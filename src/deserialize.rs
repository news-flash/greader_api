use serde::{de, Deserialize, Deserializer};
use serde_json::Value;

/// Deserialize Number or String to Option<i64>
pub fn to_optional_i64<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<i64>, D::Error> {
    Ok(match Value::deserialize(deserializer)? {
        Value::Null => None,
        Value::String(s) => Some(s.parse().map_err(de::Error::custom)?),
        Value::Number(num) => Some(
            num.as_i64()
                .ok_or_else(|| de::Error::custom("Invalid ID"))?,
        ),
        _ => return Err(de::Error::custom("wrong type")),
    })
}
